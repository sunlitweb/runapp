<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::model('run', 'Run');


//Show Pages
Route::get('/', 'RunsController@index');
Route::get('/create', 'RunsController@create');
Route::get('/edit/{run}', 'RunsController@edit');
Route::get('/delete/{run}', 'RunsController@delete');


//Handle Forms
Route::post('/create', 'RunsController@handleCreate');
Route::post('/edit', 'RunsController@handleEdit');
Route::post('/delete', 'RunsController@handleDelete');


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <style type="text/css">
    .demo-pricing {
    	margin-top: 10px;
    	margin-right: 10px;
    	padding: 14px 26px;
    	font-size: 14px;
    	line-height: 100%;
    	width: 200px;
    	height: 200px;
    	text-shadow: 0 1px rgba(0, 0, 0, 0.4);
    	color: #fff;
    	display:inline-block;
    	vertical-align: middle;
    	text-align: center;
    	cursor: pointer;
    	font-weight: bold;
    	transition: background 0.1s ease-in-out;
    	-webkit-transition: background 0.1s ease-in-out;
    	-moz-transition: background 0.1s ease-in-out;
    	-ms-transition: background 0.1s ease-in-out;
    	-o-transition: background 0.1s ease-in-out;
    	text-shadow: 0 1px rgba(0, 0, 0, 0.3);
    	color: #fff;
    	-webkit-border-radius: 3px;
    	-moz-border-radius: 3px;
    	border-radius: 3px;
    	font-family: 'Helvetica Neue', Helvetica, sans-serif;
    }
    .demo-pricing:active {
    	padding-top: 15px;
            margin-bottom: -1px;
    }
    .demo-pricing, .demo-pricing:hover, .demo-pricing:active {
    	outline: 0 none;
    	text-decoration: none;
    	color: #fff;
    }

    .demo-pricing-1 {
    	background-color: #3fb8e8;
    	box-shadow: 0px 3px 0px 0px #3293ba;
    }
    .demo-pricing-1:hover {
    	background-color: #1baae3;
    }
    .demo-pricing-1:active {
    	box-shadow: 0px 1px 0px 0px #3293ba;
    }


    .demo-pricing-2 {
    	background-color: #f06060;
    	box-shadow: 0px 3px 0px 0px #cd1313;
    }
    .demo-pricing-2:hover {
    	background-color: #ed4444;
    }
    .demo-pricing-2:active {
    	box-shadow: 0px 1px 0px 0px #cd1313;
    }

    .demo-pricing-3 {
    	background-color: #ff6a80;
    	box-shadow: 0px 3px 0px 0px #da0020;
    }
    .demo-pricing-3:hover {
    	background-color: #ff566f;
    }
    .demo-pricing-3:active {
    	box-shadow: 0px 1px 0px 0px #da0020;
    }
    </style>
    {{ HTML::style('jumbotron-narrow.css') }}
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

    <div class="container">


          <div class="jumbotron">
            <h1>Run-Fly.</h1>

            <p><img src="{{ asset('images/runapp_logo.png') }}"> </p>
            <p><small>A Personal Run Tracker for Erica Roman</small></p>
          </div>


            <div class="main">


             @yield('main')


             </div>






          <div class="footer">
            <p>&copy; <small><strong>Run-Fly.</strong> 2014 Made with love, for Erica Roman.</small></p>
          </div>

        </div> <!-- /container -->





    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>

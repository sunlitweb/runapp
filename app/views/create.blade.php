
@extends('master')

@section('main')

<a href="{{ action('RunsController@index') }}"><h2><span class="glyphicon glyphicon-chevron-left"></span></h2></a>
<div class="panel panel-default">
  <div class="panel-body">
        <form action="{{ action('RunsController@handleCreate') }}" method="post" role="form">
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" class="form-control" name="title" placeholder="Title" />
            </div>
            <div class="form-group">
                <label for="miles">Miles</label>
                <input type="text" class="form-control" name="miles" placeholder="Miles" />
            </div>
            <div class="form-group">
                <label for="time">Time</label>
                <input type="text" class="form-control" name="time" placeholder="Time" />
            </div>
               <input type="submit" value="Create" class="btn btn-primary" />
        </form>

  </div>
</div>



@stop

@extends('master')

@section('main')

<a href="{{ action('RunsController@index') }}"><h2><span class="glyphicon glyphicon-chevron-left"></span></h2></a>
<div class="panel panel-default">
  <div class="panel-body">
    <div class="panel-heading">Edit</div>
        <form action="{{ action('RunsController@handleEdit') }}" method="post" role="form">
            <input type="hidden" name="id" value="{{ $run->id }}" />
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" class="form-control" name="title" value="{{ $run->title }}" />
            </div>
            <div class="form-group">
                 <label for="title">Miles</label>
                 <input type="text" class="form-control" name="miles" value="{{ $run->miles }}" />
            </div>
            <div class="form-group">
                 <label for="title">Time</label>
                 <input type="text" class="form-control" name="time" value="{{ $run->time }}" />
            </div>

           <input type="submit" class="btn btn-primary" value="Save" />

        </form>

  </div>
</div>



@stop
@extends('master')

@section('main')

<a href="{{ action('RunsController@index') }}"><h2><span class="glyphicon glyphicon-chevron-left"></span></h2></a>
<div class="panel panel-default">
  <div class="panel-body">
    <div class="panel-heading"><div class="alert alert-danger" role="alert">Delete {{$run->title }} | Are you sure?</div></div>
        <form action="{{ action('RunsController@handleDelete') }}" method="post" role="form">
           <input type="hidden" name="run" value="{{ $run->id }}" />
           <input type="submit" class="btn btn-danger" value="Yes!" />

        </form>

  </div>
</div>



@stop
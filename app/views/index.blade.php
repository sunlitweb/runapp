@extends('master')

@section('main')

<div class="row marketing">
            <div class="col-md-3 col-md-offset-1">
              <a href="{{ action('RunsController@index') }}" class="demo-pricing demo-pricing-1">View Runs</a>
            </div>

            <div class="col-md-3 col-md-offset-3">
               <a href="{{ action('RunsController@create') }}" class="demo-pricing demo-pricing-1">Create a run</a>
            </div>




</div>

<div class="panel panel-default">
  <div class="panel-body">
    @if ($runs->isEmpty())
        <h2>Please create new run Erica! You're running on empty!</h2>
    @else
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Miles</th>
                    <th>Time</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                @foreach($runs as $run)
                <tr>
                    <td>{{ $run->title }}</td>
                    <td>{{ $run->miles }}</td>
                    <td>{{ $run->time }}</td>
                    <td>
                    <a href="{{ action('RunsController@edit', $run->id) }}">
                        <button type="button" class="btn btn-info btn-xs">
                            <span class="glyphicon glyphicon-pencil"></span> Edit
                        </button>
                    </a>
                    </td>
                    <td>
                    <a href="{{ action('RunsController@delete', $run->id) }}">
                        <button type="button" class="btn btn-danger btn-xs">
                            <span class="glyphicon glyphicon-trash"></span> Delete
                        </button>
                    </a>
                    </td>
                 </tr>

                @endforeach

            </tbody>

        </table>
        @endif


  </div>

</div>





@stop



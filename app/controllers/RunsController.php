

<?php

Class RunsController extends BaseController {

    public function index(){

        //Show a listing of all runs
        $runs = Run::all();
        return View::make('index', compact('runs'));

    }

    public function Create() {

        //Show the create runs form
        return View::make('create');
    }

    public function handleCreate() {

        //Handles the create form
        $run = new Run;
        $run->title = Input::get('title');
        $run->miles = Input::get('miles');
        $run->time  = Input::get('time');
        $run->save();

        return Redirect::action('RunsController@index');
    }

    public function edit(Run $run) {


        //Show the edit run form
        return View::make('edit', compact('run'));
    }

    public function handleEdit() {
         //Handles the edit run form
         $run = Run::findOrFail(Input::get('id'));
         $run->title = Input::get('title');
        $run->miles = Input::get('miles');
        $run->time  = Input::get('time');
        $run->save();

        return Redirect::action('RunsController@index');

    }


    public function delete(Run $run) {

        //Show the edit run form
        return View::make('delete', compact('run'));
    }

    public function handleDelete() {
        //Handles the edit run form
        $id = Input::get('run');
        $run = Run::findOrFail($id);
        $run->delete();

        return Redirect::action('RunsController@index');

    }

}